package de.dr_schnauzer.project_unstable.mms.ic2;

import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.helpers.LogHelper;
import ic2.api.recipe.IRecipeInput;
import ic2.api.recipe.MachineRecipe;
import ic2.core.recipe.RecipeInputFactory;
import minetweaker.api.item.IIngredient;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class IC2Recipe {

    private MachineRecipe<IRecipeInput, Collection<ItemStack>> recipe;
    private List<IRecipeInput> inputs = new ArrayList<>();
    private List<ItemStack> outputs = new ArrayList<>();
    private boolean replace;
    private RecipeInputFactory factory = new RecipeInputFactory();

    public IC2Recipe(MachineRecipe<IRecipeInput, Collection<ItemStack>> recipe) {
        this.inputs.add(recipe.getInput());
        this.outputs.addAll(recipe.getOutput());
        this.recipe = recipe;
    }

    public IC2Recipe(ItemStack[] output, IIngredient input, boolean replace) {
        this(output, input, replace, null);
    }

    public IC2Recipe(ItemStack[] output, IIngredient input, boolean replace, NBTTagCompound meta) {
        this(output, new IIngredient[]{input}, replace, meta);
    }

    public IC2Recipe(ItemStack[] output, IIngredient[] input, boolean replace) {
        this(output, input, replace, null);
    }

    public IC2Recipe(ItemStack[] output, IIngredient[] input, boolean replace, NBTTagCompound meta) {

        for (IIngredient ingredient : input) {
            addRecipeInput(InputHelper.toObject(ingredient), ingredient.getAmount());
        }
        if (inputs.isEmpty()) {
            LogHelper.logError("Error while creating Recipe input, IIngredient[] was empty");
        } else {
            Collections.addAll(this.outputs, output);
            this.replace = replace;
        }
        recipe = new MachineRecipe<>(getInput(), getOutputs(), meta);
    }

    private void addRecipeInput(Object object, int amount) {
        if (object instanceof String && !((String) object).isEmpty()) {
            inputs.add(factory.forOreDict((String) object, amount));
        } else if (object instanceof ItemStack) {
            inputs.add(factory.forStack((ItemStack) object, amount));
        } else if (object instanceof List && !((List) object).isEmpty()) {
            for (ItemStack stack : (List<ItemStack>) object) {
                inputs.add(factory.forStack(stack, amount));
            }
        }
    }

    public boolean isValid() {
        return !inputs.isEmpty();
    }

    public IRecipeInput getInput() {
        if (inputs.size() == 1) {
            return inputs.get(0);
        }
        return factory.forAny(inputs);
    }

    public List<ItemStack> getOutputs() {
        return outputs;
    }

    public NBTTagCompound getMeta() {
        return recipe.getMetaData();
    }

    public MachineRecipe<IRecipeInput, Collection<ItemStack>> getRecipe(){
        return recipe;
    }
}
