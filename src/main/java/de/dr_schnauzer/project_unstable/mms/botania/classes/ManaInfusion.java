package de.dr_schnauzer.project_unstable.mms.botania.classes;

import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.helpers.StackHelper;
import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;
import de.dr_schnauzer.project_unstable.mms.util.JEIHelper;
import mezz.jei.ingredients.Ingredients;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import net.minecraft.block.Block;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;
import vazkii.botania.api.BotaniaAPI;
import vazkii.botania.api.recipe.RecipeManaInfusion;
import vazkii.botania.client.integration.jei.manapool.ManaPoolRecipeWrapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@ZenClass("mods.botania.ManaInfusion")
public class ManaInfusion {

    private static final String category = "botania.manaPool";

    @ZenMethod
    public static void add(IItemStack output, IIngredient input, int mana, IItemStack cataclyst) {
        RecipeManaInfusion recipe = new RecipeManaInfusion(InputHelper.toStack(output), InputHelper.toObject(input), mana);
        if (InputHelper.isABlock(cataclyst)) {
            recipe.setCatalyst(Block.getBlockFromItem(InputHelper.toStack(cataclyst).getItem()).getDefaultState());
        }
        MineTweakerAPI.apply(new Add(Collections.singletonList(recipe)));
    }

    @ZenMethod
    public static void remove(IItemStack output) {
        List<RecipeManaInfusion> recipesToRemove = new ArrayList<>();
        for (RecipeManaInfusion recipe : BotaniaAPI.manaInfusionRecipes) {
            if (StackHelper.areEqual(InputHelper.toStack(output), recipe.getOutput())) {
                recipesToRemove.add(recipe);
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove));
        }
    }

    private static class Add extends BaseListAddition<RecipeManaInfusion> {

        private HashMap<RecipeManaInfusion, ManaPoolRecipeWrapper> recipeJEI = new HashMap<>();

        protected Add(List<RecipeManaInfusion> list) {
            super("Mana Infusion", list);
            for (RecipeManaInfusion recipe : list) {
                recipeJEI.put(recipe, new ManaPoolRecipeWrapper(recipe));
                BotaniaAPI.manaInfusionRecipes.add(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }

        @Override
        protected String getRecipeInfo(RecipeManaInfusion recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(RecipeManaInfusion recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (RecipeManaInfusion recipe : recipeJEI.keySet()) {
                BotaniaAPI.manaInfusionRecipes.remove(recipe);
                JEIHelper.removeRecipe(recipeJEI.get(recipe), category);
            }
        }
    }

    private static class Remove extends BaseListRemoval<RecipeManaInfusion> {

        private HashMap<RecipeManaInfusion, ManaPoolRecipeWrapper> recipeJEI = new HashMap<>();

        protected Remove(List<RecipeManaInfusion> list) {
            super("Mana Infusion", list);
            for (RecipeManaInfusion recipe : list) {

                for (ManaPoolRecipeWrapper recipeJEI : JEIHelper.<ManaPoolRecipeWrapper>getRecipeWrappers(category)) {
                    Ingredients ingredients = new Ingredients();
                    recipeJEI.getIngredients(ingredients);

                    boolean flag = false;
                    for (List<ItemStack> stacks : ingredients.getOutputs(ItemStack.class)) {
                        for (ItemStack stack : stacks) {
                            if (StackHelper.areEqual(recipe.getOutput(), stack)) {
                                this.recipeJEI.put(recipe, recipeJEI);
                                BotaniaAPI.manaInfusionRecipes.remove(recipe);
                                JEIHelper.removeRecipe(recipeJEI, category);
                                flag = true;
                                break;
                            }
                        }
                        if (flag) break;
                    }
                }
            }
        }

        @Override
        protected String getRecipeInfo(RecipeManaInfusion recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(RecipeManaInfusion recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (RecipeManaInfusion recipe : recipeJEI.keySet()) {
                BotaniaAPI.manaInfusionRecipes.remove(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }
    }
}
