package de.dr_schnauzer.project_unstable.mms.cofh.classes;

import cofh.thermalexpansion.util.managers.machine.SmelterManager;
import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.helpers.LogHelper;
import com.blamejared.mtlib.helpers.StackHelper;
import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;
import de.dr_schnauzer.project_unstable.mms.util.JEIHelper;
import mezz.jei.Internal;
import mezz.jei.ingredients.Ingredients;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import stanhebben.zenscript.annotations.Optional;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;
import cofh.thermalexpansion.plugins.jei.crafting.smelter.SmelterRecipeWrapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@ZenClass("mods.thermalexpansion.Smelter")
public class InductionSmelter {

    private static final String category = "thermalexpansion.smelter";

    //TODO: change Input to IIngredient
    @ZenMethod
    public static void add(IItemStack output, @Optional IItemStack secondaryOutput, IItemStack input, @Optional IItemStack secondaryInput, int secondaryChance, int energy) {
        SmelterManager.SmelterRecipe recipe = SmelterManager.addRecipe(
                energy,
                InputHelper.toStack(input),
                InputHelper.toStack(secondaryInput),
                InputHelper.toStack(output),
                InputHelper.toStack(secondaryOutput),
                secondaryChance
        );
        if (recipe == null) {
            LogHelper.logError("Induction Smelter recipe for Input <" + InputHelper.toStack(input).getDisplayName() + "> already exists");
        }
        MineTweakerAPI.apply(new Add(Collections.singletonList(recipe)));
    }

    @ZenMethod
    public static void removeByOutput(IItemStack output, @Optional IItemStack secondaryOutput) {
        List<SmelterManager.SmelterRecipe> recipesToRemove = new ArrayList<>();
        for (SmelterManager.SmelterRecipe recipe : SmelterManager.getRecipeList()) {
            if (StackHelper.areEqual(recipe.getPrimaryOutput(), InputHelper.toStack(output))) {
                if (ItemStack.areItemStacksEqual(recipe.getSecondaryOutput(), InputHelper.toStack(secondaryOutput))) {
                    recipesToRemove.add(recipe);
                }
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove));
        }
    }

    @ZenMethod
    public static void removeByInput(IIngredient input, @Optional IIngredient secondaryInput) {
        List<SmelterManager.SmelterRecipe> recipesToRemove = new ArrayList<>();
        for (SmelterManager.SmelterRecipe recipe : SmelterManager.getRecipeList()) {
            for (IItemStack input1 : input.getItems()) {
                for (IItemStack input2 : secondaryInput.getItems()) {
                    if (StackHelper.areEqual(recipe.getPrimaryInput(), InputHelper.toStack(input1)) && StackHelper.areEqual(recipe.getSecondaryInput(), InputHelper.toStack(input2))) {
                        recipesToRemove.add(recipe);
                    }
                }
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove));
        }
    }

    private static class Add extends BaseListAddition<SmelterManager.SmelterRecipe> {

        private HashMap<SmelterManager.SmelterRecipe, SmelterRecipeWrapper> recipeJEI = new HashMap<>();

        protected Add(List<SmelterManager.SmelterRecipe> list) {
            super("Induction Smelter", list);

            if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
                for (SmelterManager.SmelterRecipe recipe : list) {
                    recipeJEI.put(recipe, new SmelterRecipeWrapper(Internal.getHelpers().getGuiHelper(), recipe));
                    //already registered recipe ... SmelterManager.getRecipeList();
                    JEIHelper.addRecipe(recipeJEI.get(recipe), category);
                }
            }
        }

        @Override
        protected String getRecipeInfo(SmelterManager.SmelterRecipe recipe) {
            return recipe.getPrimaryOutput().toString() + " + " + recipe.getSecondaryOutput().toString();
        }

        @Override
        public String getJEICategory(SmelterManager.SmelterRecipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (SmelterManager.SmelterRecipe recipe : recipeJEI.keySet()) {
                SmelterManager.removeRecipe(recipe.getPrimaryInput(), recipe.getSecondaryInput());
                JEIHelper.removeRecipe(recipeJEI.get(recipe), category);
            }
        }
    }

    private static class Remove extends BaseListRemoval<SmelterManager.SmelterRecipe> {

        private HashMap<SmelterManager.SmelterRecipe, SmelterRecipeWrapper> recipeJEI = new HashMap<>();

        protected Remove(List<SmelterManager.SmelterRecipe> list) {
            super("Induction Smelter", list);
            for (SmelterManager.SmelterRecipe recipe : list) {
                for (SmelterRecipeWrapper recipeJEI : JEIHelper.<SmelterRecipeWrapper>getRecipeWrappers(category)) {
                    Ingredients ingredients = new Ingredients();
                    recipeJEI.getIngredients(ingredients);

                    boolean flag = false;
                    //Inputs
                    {
                        List<List<ItemStack>> stacks = ingredients.getInputs(ItemStack.class);
                        for (ItemStack primary : stacks.get(0)) {
                            if (StackHelper.areEqual(recipe.getPrimaryInput(), primary)) {
                                flag = true;
                                break;
                            }
                        }
                        if (!flag) continue;
                        flag = false;
                        if (stacks.get(1).isEmpty()) {
                            flag = !recipe.getSecondaryInput().func_190926_b();
                        } else {
                            for (ItemStack secondary : stacks.get(1)) {
                                if (ItemStack.areItemStacksEqual(recipe.getSecondaryInput(), secondary)) {
                                    flag = true;
                                    break;
                                }
                            }
                        }
                        if (!flag) continue;
                    }
                    //Outputs
                    {
                        List<List<ItemStack>> stacks = ingredients.getOutputs(ItemStack.class);
                        flag = false;
                        for (ItemStack primary : stacks.get(0)) {
                            if (StackHelper.areEqual(recipe.getPrimaryOutput(), primary)) {
                                flag = true;
                                break;
                            }
                        }
                        if (!flag) continue;
                        flag = false;
                        if (stacks.get(1).isEmpty()) {
                            flag = !recipe.getSecondaryOutput().func_190926_b();
                        } else {
                            for (ItemStack secondary : stacks.get(1)) {
                                if (ItemStack.areItemStacksEqual(recipe.getSecondaryOutput(), secondary)) {
                                    flag = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (flag) {
                        this.recipeJEI.put(recipe, recipeJEI);
                        SmelterManager.removeRecipe(recipe.getPrimaryInput(), recipe.getSecondaryInput());
                        JEIHelper.removeRecipe(recipeJEI, category);
                    }
                }
            }
        }

        @Override
        protected String getRecipeInfo(SmelterManager.SmelterRecipe recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(SmelterManager.SmelterRecipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (SmelterManager.SmelterRecipe recipe : recipeJEI.keySet()) {
                SmelterManager.addRecipe(recipe.getEnergy(), recipe.getPrimaryInput(), recipe.getSecondaryInput(), recipe.getPrimaryOutput(), recipe.getSecondaryOutput(), recipe.getSecondaryOutputChance());
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }
    }
}
