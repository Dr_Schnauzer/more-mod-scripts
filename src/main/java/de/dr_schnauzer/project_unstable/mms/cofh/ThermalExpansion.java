package de.dr_schnauzer.project_unstable.mms.cofh;

import de.dr_schnauzer.project_unstable.mms.cofh.classes.*;
import minetweaker.MineTweakerAPI;

public class ThermalExpansion {

    public static void register() {

        MineTweakerAPI.registerClass(InductionSmelter.class);
        MineTweakerAPI.registerClass(Pulverizer.class);
        MineTweakerAPI.registerClass(RedstoneFurnace.class);
        MineTweakerAPI.registerClass(FluidTransposer.class);
        MineTweakerAPI.registerClass(CentrifugalSeperator.class);
    }
}
