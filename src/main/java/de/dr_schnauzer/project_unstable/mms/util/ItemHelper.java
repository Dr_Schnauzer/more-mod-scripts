package de.dr_schnauzer.project_unstable.mms.util;

import com.blamejared.mtlib.helpers.StackHelper;
import net.minecraft.item.ItemStack;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ItemHelper {

    public static boolean areStacksEqual(Collection<ItemStack> a, Collection<ItemStack> b) {
        List<ItemStack> itemStacks = new ArrayList<>();
        itemStacks.addAll(a);

        for (ItemStack stack : b) {
            ItemStack remove = null;
            for (ItemStack itemStack : itemStacks) {
                if (StackHelper.areEqual(itemStack, stack)) {
                    remove = itemStack;
                    break;
                }
            }
            if (remove == null) return false;
            itemStacks.remove(remove);
        }
        return itemStacks.isEmpty();
    }
}
