package de.dr_schnauzer.project_unstable.mms.ic2.classes;

import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.helpers.LogHelper;
import com.blamejared.mtlib.helpers.StackHelper;
import de.dr_schnauzer.project_unstable.mms.ic2.BaseMachineRecipe;
import de.dr_schnauzer.project_unstable.mms.ic2.IC2Recipe;
import ic2.api.recipe.IBasicMachineRecipeManager;
import ic2.api.recipe.IRecipeInput;
import ic2.api.recipe.MachineRecipe;
import ic2.api.recipe.Recipes;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@ZenClass("mods.ic2.OreWasher")
public class OreWasher extends BaseMachineRecipe {

    private static final String category = "ore_washing_plant";
    private static final IBasicMachineRecipeManager manager = Recipes.oreWashing;

    @ZenMethod
    public static void add(IItemStack output, IIngredient input) {
        IC2Recipe ic2Recipe = new IC2Recipe(new ItemStack[]{InputHelper.toStack(output)}, input, false);
        if (ic2Recipe.isValid()) {
            MineTweakerAPI.apply(new Add(Collections.singletonList(ic2Recipe), manager, category));
        }
    }

    @ZenMethod
    public static void add(IItemStack[] output, IIngredient input) {
        IC2Recipe ic2Recipe = new IC2Recipe(InputHelper.toStacks(output), input, false);
        if (ic2Recipe.isValid()) {
            MineTweakerAPI.apply(new Add(Collections.singletonList(ic2Recipe), manager, category));
        }
    }

    @ZenMethod
    public static void removeByOutput(IIngredient output) {
        List<IC2Recipe> recipesToRemove = new ArrayList<>();
        for (MachineRecipe<IRecipeInput, Collection<ItemStack>> recipe : manager.getRecipes()) {
            boolean match = false;
            for (IItemStack outputStack : output.getItems()) {
                for (ItemStack stack : recipe.getOutput()) {
                    if (StackHelper.areEqual(InputHelper.toStack(outputStack), stack)) {
                        recipesToRemove.add(new IC2Recipe(recipe));
                        match = true;
                        break;
                    }
                }
                if (match) break;
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove, manager, category));
        } else {
            LogHelper.logError("no matching Ore Washer recipe found for output " + output);
        }
    }

    @ZenMethod
    public static void removeByInput(IIngredient input) {
        List<IC2Recipe> recipesToRemove = new ArrayList<>();
        for (MachineRecipe<IRecipeInput, Collection<ItemStack>> recipe : manager.getRecipes()) {
            for (IItemStack inputStack : input.getItems()) {
                for (ItemStack stack : recipe.getInput().getInputs()) {
                    if (StackHelper.areEqual(stack, InputHelper.toStack(inputStack))) {
                        recipesToRemove.add(new IC2Recipe(recipe));
                    }
                }
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove, manager, category));
        } else {
            LogHelper.logError("no matching Ore Washer recipe found for input " + input);
        }
    }
}
