package de.dr_schnauzer.project_unstable.mms.botania.classes;

import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;
import de.dr_schnauzer.project_unstable.mms.util.ItemHelper;
import de.dr_schnauzer.project_unstable.mms.util.JEIHelper;
import mezz.jei.ingredients.Ingredients;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;
import vazkii.botania.api.BotaniaAPI;
import vazkii.botania.api.recipe.RecipeElvenTrade;
import vazkii.botania.client.integration.jei.elventrade.ElvenTradeRecipeWrapper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@ZenClass("mods.botania.ElvenTrade")
public class ElvenTrade {

    private static final String category = "botania.elvenTrade";

    @ZenMethod
    public static void add(IItemStack output, IIngredient input) {
        RecipeElvenTrade recipe = new RecipeElvenTrade(new ItemStack[] {InputHelper.toStack(output)}, InputHelper.toObject(input));
        MineTweakerAPI.apply(new Add(Collections.singletonList(recipe)));
    }

    @ZenMethod
    public static void add(IItemStack[] output, IIngredient[] input) {
        RecipeElvenTrade recipe = new RecipeElvenTrade(InputHelper.toStacks(output), InputHelper.toObjects(input));
        MineTweakerAPI.apply(new Add(Collections.singletonList(recipe)));
    }

    @ZenMethod
    public static void remove(IItemStack[] output) {
        List<RecipeElvenTrade> recipesToRemove = new ArrayList<>();
        for (RecipeElvenTrade recipe : BotaniaAPI.elvenTradeRecipes) {
            List<ItemStack> stacks = new ArrayList<>();
            Collections.addAll(stacks, InputHelper.toStacks(output));
            if (ItemHelper.areStacksEqual(recipe.getOutputs(), stacks)) {
                recipesToRemove.add(recipe);
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove));
        }
    }

    private static class Add extends BaseListAddition<RecipeElvenTrade> {

        private HashMap<RecipeElvenTrade, ElvenTradeRecipeWrapper> recipeJEI = new HashMap<>();

        protected Add(List<RecipeElvenTrade> list) {
            super("Elven Trade", list);
            for (RecipeElvenTrade recipe : list) {
                recipeJEI.put(recipe, new ElvenTradeRecipeWrapper(recipe));
                BotaniaAPI.elvenTradeRecipes.add(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }

        @Override
        protected String getRecipeInfo(RecipeElvenTrade recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(RecipeElvenTrade recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (RecipeElvenTrade recipe : recipeJEI.keySet()) {
                BotaniaAPI.elvenTradeRecipes.remove(recipe);
                JEIHelper.removeRecipe(recipeJEI.get(recipe), category);
            }
        }
    }

    private static class Remove extends BaseListRemoval<RecipeElvenTrade> {

        private HashMap<RecipeElvenTrade, ElvenTradeRecipeWrapper> recipeJEI = new HashMap<>();

        protected Remove(List<RecipeElvenTrade> list) {
            super("Elven Trade", list);
            for (RecipeElvenTrade recipe : list) {

                for (ElvenTradeRecipeWrapper recipeJEI : JEIHelper.<ElvenTradeRecipeWrapper>getRecipeWrappers(category)) {
                    Ingredients ingredients = new Ingredients();
                    recipeJEI.getIngredients(ingredients);

                    for (List<ItemStack> stacks : ingredients.getOutputs(ItemStack.class)) {
                        if (ItemHelper.areStacksEqual(recipe.getOutputs(), stacks)) {
                            this.recipeJEI.put(recipe, recipeJEI);
                            BotaniaAPI.elvenTradeRecipes.remove(recipe);
                            JEIHelper.removeRecipe(recipeJEI, category);
                            break;
                        }
                    }
                }
            }
        }

        @Override
        protected String getRecipeInfo(RecipeElvenTrade recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(RecipeElvenTrade recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (RecipeElvenTrade recipe : recipeJEI.keySet()) {
                BotaniaAPI.elvenTradeRecipes.remove(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }
    }
}
