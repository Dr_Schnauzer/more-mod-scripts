package de.dr_schnauzer.project_unstable.mms.cofh.classes;

import cofh.thermalexpansion.plugins.jei.crafting.centrifuge.CentrifugeRecipeWrapper;
import cofh.thermalexpansion.util.managers.machine.CentrifugeManager;
import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.helpers.LogHelper;
import com.blamejared.mtlib.helpers.StackHelper;
import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;
import de.dr_schnauzer.project_unstable.mms.util.ItemHelper;
import de.dr_schnauzer.project_unstable.mms.util.JEIHelper;
import mezz.jei.Internal;
import mezz.jei.ingredients.Ingredients;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import minetweaker.api.liquid.ILiquidStack;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

import java.util.*;

@ZenClass("mods.thermalexpansion.Centrifuge")
public class CentrifugalSeperator {

    private static final String category = "thermalexpansion.centrifuge";

    @ZenMethod
    public static void add(IItemStack[] outputs, int[] chances, ILiquidStack liquid, IIngredient input, int energy) {
        List<CentrifugeManager.CentrifugeRecipe> recipes = new ArrayList<>();

        List<ItemStack> outputStacks = new ArrayList<>();
        Collections.addAll(outputStacks, InputHelper.toStacks(outputs));

        List<Integer> outputChances = new ArrayList<>();
        for (int chance : chances) {
            outputChances.add(chance);
        }

        for (IItemStack stack : input.getItems()) {
            CentrifugeManager.CentrifugeRecipe recipe = CentrifugeManager.addRecipe(
                    energy,
                    InputHelper.toStack(stack),
                    outputStacks,
                    outputChances,
                    InputHelper.toFluid(liquid)
            );
            if (recipe == null) {
                LogHelper.logError("Centrifugal Seperator recipe for Input <" + stack.getDisplayName() + "> already exists");
            }
            recipes.add(recipe);
        }
        if (!recipes.isEmpty()) {
            MineTweakerAPI.apply(new CentrifugalSeperator.Add(recipes));
        } else {
            LogHelper.logError("no matching recipe found");
        }
    }

    @ZenMethod
    public static void removeByOutput(IItemStack[] outputs) {
        List<ItemStack> outputStacks = new ArrayList<>();
        Collections.addAll(outputStacks, InputHelper.toStacks(outputs));

        List<CentrifugeManager.CentrifugeRecipe> recipesToRemove = new ArrayList<>();
        for (CentrifugeManager.CentrifugeRecipe recipe : CentrifugeManager.getRecipeList()) {
            if (ItemHelper.areStacksEqual(recipe.getOutput(), outputStacks)) {
                recipesToRemove.add(recipe);
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove));
        }
    }

    @ZenMethod
    public static void removeByInput(IIngredient input) {
        List<CentrifugeManager.CentrifugeRecipe> recipesToRemove = new ArrayList<>();
        for (CentrifugeManager.CentrifugeRecipe recipe : CentrifugeManager.getRecipeList()) {
            for (IItemStack stack : input.getItems()) {
                if (StackHelper.areEqual(recipe.getInput(), InputHelper.toStack(stack))) {
                    recipesToRemove.add(recipe);
                }
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove));
        }
    }

    private static class Add extends BaseListAddition<CentrifugeManager.CentrifugeRecipe> {

        private HashMap<CentrifugeManager.CentrifugeRecipe, CentrifugeRecipeWrapper> recipeJEI = new HashMap<>();

        protected Add(List<CentrifugeManager.CentrifugeRecipe> list) {
            super("Centrifuge", list);

            if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
                for (CentrifugeManager.CentrifugeRecipe recipe : list) {
                    recipeJEI.put(recipe, new CentrifugeRecipeWrapper(Internal.getHelpers().getGuiHelper(), recipe));
                    //already registered recipe ... CentrifugeManager.getRecipeList();
                    JEIHelper.addRecipe(recipeJEI.get(recipe), category);
                }
            }
        }

        @Override
        protected String getRecipeInfo(CentrifugeManager.CentrifugeRecipe recipe) {
            return Arrays.toString(recipe.getOutput().toArray());
        }

        @Override
        public String getJEICategory(CentrifugeManager.CentrifugeRecipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (CentrifugeManager.CentrifugeRecipe recipe : recipeJEI.keySet()) {
                CentrifugeManager.removeRecipe(recipe.getInput());
                JEIHelper.removeRecipe(recipeJEI.get(recipe), category);
            }
        }
    }

    private static class Remove extends BaseListRemoval<CentrifugeManager.CentrifugeRecipe> {

        private HashMap<CentrifugeManager.CentrifugeRecipe, CentrifugeRecipeWrapper> recipeJEI = new HashMap<>();

        protected Remove(List<CentrifugeManager.CentrifugeRecipe> list) {
            super("Centrifuge", list);
            for (CentrifugeManager.CentrifugeRecipe recipe : list) {

                for (CentrifugeRecipeWrapper recipeJEI : JEIHelper.<CentrifugeRecipeWrapper>getRecipeWrappers(category)) {
                    Ingredients ingredients = new Ingredients();
                    recipeJEI.getIngredients(ingredients);

                    boolean flag = false;
                    for (List<FluidStack> fluids : ingredients.getOutputs(FluidStack.class)) {
                        for (FluidStack fluid : fluids) {
                            if (StackHelper.areEqualOrNull(fluid, recipe.getFluid())) {
                                flag = true;
                                break;
                            }
                        }
                    }
                    if (!flag) continue;
                    flag = false;
                    for (List<ItemStack> stacks : ingredients.getInputs(ItemStack.class)) {
                        for (ItemStack stack : stacks) {
                            if (StackHelper.areEqualOrNull(stack, recipe.getInput())) {
                                flag = true;
                                break;
                            }
                        }
                    }
                    if (flag) {
                        List<List<ItemStack>> stacks = ingredients.getOutputs(ItemStack.class);

                        for (List<ItemStack> stackList : stacks) {
                            boolean match = false;
                            for (ItemStack stack : stackList) {
                                for (ItemStack recipeStack : recipe.getOutput()) {
                                    if (StackHelper.areEqualOrNull(stack, recipeStack)) {
                                        match = true;
                                        break;
                                    }
                                }
                                if (match) break;
                            }
                            if (match) {
                                flag = true;
                            } else {
                                flag = false;
                                break;
                            }
                        }
                        if (!flag) continue;
                        this.recipeJEI.put(recipe, recipeJEI);
                        CentrifugeManager.removeRecipe(recipe.getInput());
                        JEIHelper.removeRecipe(recipeJEI, category);
                        break;
                    }

                }
            }
        }

        @Override
        protected String getRecipeInfo(CentrifugeManager.CentrifugeRecipe recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(CentrifugeManager.CentrifugeRecipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (CentrifugeManager.CentrifugeRecipe recipe : recipeJEI.keySet()) {
                CentrifugeManager.addRecipe(recipe.getEnergy(), recipe.getInput(), recipe.getOutput(), recipe.getChance(), recipe.getFluid());
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }
    }
}
