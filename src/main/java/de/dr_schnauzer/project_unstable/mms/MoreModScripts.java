package de.dr_schnauzer.project_unstable.mms;

import de.dr_schnauzer.project_unstable.mms.bloodmagic.BloodMagic;
import de.dr_schnauzer.project_unstable.mms.botania.Botania;
import de.dr_schnauzer.project_unstable.mms.cofh.ThermalExpansion;
import de.dr_schnauzer.project_unstable.mms.ic2.IC2;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = MoreModScripts.MOD_ID, name = MoreModScripts.MOD_NAME, version = MoreModScripts.MOD_VERSION)
public class MoreModScripts {

    public static final String MOD_ID = "mms";
    public static final String MOD_NAME = "More Mod Scripts";
    public static final String MOD_VERSION = "1.3";

    @Mod.Instance
    public static MoreModScripts INSTANCE;

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent event) {

    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent event) {
        if (Loader.isModLoaded("bloodmagic")) {
            BloodMagic.register();
        }
        if (Loader.isModLoaded("botania")) {
            Botania.register();
        }
        if (Loader.isModLoaded("thermalexpansion")) {
            ThermalExpansion.register();
        }
        if (Loader.isModLoaded("ic2")) {
            IC2.register();
        }
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent event) {

    }
}