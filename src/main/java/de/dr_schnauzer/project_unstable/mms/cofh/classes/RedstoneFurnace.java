package de.dr_schnauzer.project_unstable.mms.cofh.classes;

import cofh.thermalexpansion.plugins.jei.crafting.furnace.FurnaceRecipeWrapper;
import cofh.thermalexpansion.util.managers.machine.FurnaceManager;
import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.helpers.LogHelper;
import com.blamejared.mtlib.helpers.StackHelper;
import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;
import de.dr_schnauzer.project_unstable.mms.util.JEIHelper;
import mezz.jei.Internal;
import mezz.jei.ingredients.Ingredients;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@ZenClass("mods.thermalexpansion.RedstoneFurnace")
public class RedstoneFurnace {

    private static final String category = "thermalexpansion.furnace";

    @ZenMethod
    public static void add(IItemStack output, IIngredient input, int energy) {
        List<FurnaceManager.FurnaceRecipe> recipes = new ArrayList<>();
        for (IItemStack stack : input.getItems()) {
            FurnaceManager.FurnaceRecipe recipe = FurnaceManager.addRecipe(
                    energy,
                    InputHelper.toStack(stack),
                    InputHelper.toStack(output)
            );
            if (recipe == null) {
                LogHelper.logError("RedstoneFurnace recipe for Input <" + stack.getDisplayName() + "> already exists");
            }
            recipes.add(recipe);
        }
        if (!recipes.isEmpty()) {
            MineTweakerAPI.apply(new Add(recipes));
        }
    }

    @ZenMethod
    public static void removeByOutput(IIngredient output) {
        List<FurnaceManager.FurnaceRecipe> recipesToRemove = new ArrayList<>();
        for (FurnaceManager.FurnaceRecipe recipe : FurnaceManager.getRecipeList()) {
            for (IItemStack stack : output.getItems()) {
                if (StackHelper.areEqual(recipe.getOutput(), InputHelper.toStack(stack))) {
                    recipesToRemove.add(recipe);
                }
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove));
        } else {
            LogHelper.logError("no matching Redstone Furnace recipe found for output " + output);
        }
    }

    @ZenMethod
    public static void removeByInput(IIngredient input) {
        List<FurnaceManager.FurnaceRecipe> recipesToRemove = new ArrayList<>();
        for (FurnaceManager.FurnaceRecipe recipe : FurnaceManager.getRecipeList()) {
            for (IItemStack stack : input.getItems()) {
                if (StackHelper.areEqual(recipe.getInput(), InputHelper.toStack(stack))) {
                    recipesToRemove.add(recipe);
                }
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove));
        } else {
            LogHelper.logError("no matching Redstone Furnace recipe found for input " + input);
        }
    }

    private static class Add extends BaseListAddition<FurnaceManager.FurnaceRecipe> {

        private HashMap<FurnaceManager.FurnaceRecipe, FurnaceRecipeWrapper> recipeJEI = new HashMap<>();

        protected Add(List<FurnaceManager.FurnaceRecipe> list) {
            super("Redstone Furnace", list);
            if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
                for (FurnaceManager.FurnaceRecipe recipe : list) {
                    recipeJEI.put(recipe, new FurnaceRecipeWrapper(Internal.getHelpers().getGuiHelper(), recipe));
                    //already registered recipe ... FurnaceManager.getRecipeList();
                    JEIHelper.addRecipe(recipeJEI.get(recipe), category);
                }
            }
        }

        @Override
        protected String getRecipeInfo(FurnaceManager.FurnaceRecipe recipe) {
            return recipe.getOutput().toString();
        }

        @Override
        public String getJEICategory(FurnaceManager.FurnaceRecipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (FurnaceManager.FurnaceRecipe recipe : recipeJEI.keySet()) {
                FurnaceManager.removeRecipe(recipe.getInput());
                JEIHelper.removeRecipe(recipeJEI.get(recipe), category);
            }
        }
    }

    private static class Remove extends BaseListRemoval<FurnaceManager.FurnaceRecipe> {

        private HashMap<FurnaceManager.FurnaceRecipe, FurnaceRecipeWrapper> recipeJEI = new HashMap<>();

        protected Remove(List<FurnaceManager.FurnaceRecipe> list) {
            super("Redstone Furnace", list);
            for (FurnaceManager.FurnaceRecipe recipe : list) {

                for (FurnaceRecipeWrapper recipeJEI : JEIHelper.<FurnaceRecipeWrapper>getRecipeWrappers(category)) {
                    Ingredients ingredients = new Ingredients();
                    recipeJEI.getIngredients(ingredients);

                    boolean flag = false;
                    for (List<ItemStack> stacks : ingredients.getInputs(ItemStack.class)) {
                        for (ItemStack stack : stacks) {
                            if (StackHelper.areEqual(stack, recipe.getInput())) {
                                flag = true;
                                break;
                            }
                        }
                    }
                    if (flag) {
                        flag = false;
                        for (List<ItemStack> stacks : ingredients.getOutputs(ItemStack.class)) {
                            for (ItemStack stack : stacks) {
                                if (StackHelper.areEqual(stack, recipe.getOutput())) {
                                    flag = true;
                                    break;
                                }
                            }
                        }
                        if (flag) {
                            this.recipeJEI.put(recipe, recipeJEI);
                            FurnaceManager.removeRecipe(recipe.getInput());
                            JEIHelper.removeRecipe(recipeJEI, category);
                            break;
                        }
                    }

                }
            }
        }

        @Override
        protected String getRecipeInfo(FurnaceManager.FurnaceRecipe recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(FurnaceManager.FurnaceRecipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (FurnaceManager.FurnaceRecipe recipe : recipeJEI.keySet()) {
                FurnaceManager.addRecipe(recipe.getEnergy(), recipe.getInput(), recipe.getOutput());
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }
    }
}
