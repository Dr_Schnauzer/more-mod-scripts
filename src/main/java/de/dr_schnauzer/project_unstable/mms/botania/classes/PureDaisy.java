package de.dr_schnauzer.project_unstable.mms.botania.classes;

import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;
import de.dr_schnauzer.project_unstable.mms.util.JEIHelper;
import mezz.jei.ingredients.Ingredients;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IItemStack;
import minetweaker.api.oredict.IOreDictEntry;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.Optional;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;
import vazkii.botania.api.BotaniaAPI;
import vazkii.botania.api.recipe.RecipePureDaisy;
import vazkii.botania.client.integration.jei.puredaisy.PureDaisyRecipeWrapper;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@ZenClass("mods.botania.PureDaisy")
public class PureDaisy {

    private static final String category = "botania.pureDaisy";

    @ZenMethod
    public static void add(IItemStack blockOutput, IItemStack blockInput) {
        add(blockOutput, blockInput, 150);
    }

    @ZenMethod
    public static void add(IItemStack blockOutput, IItemStack blockInput, int time) {
        if (InputHelper.isABlock(blockOutput)) {
            IBlockState output = Block.getBlockFromItem(InputHelper.toStack(blockOutput).getItem()).getDefaultState();

            if (InputHelper.isABlock(blockInput)) {
                Object input = Block.getBlockFromItem(InputHelper.toStack(blockInput).getItem());

                RecipePureDaisy recipe = new RecipePureDaisy(input, output, time);
                MineTweakerAPI.apply(new Add(Collections.singletonList(recipe)));
            } else {
                MineTweakerAPI.logError("input is not a block");
            }
        } else {
            MineTweakerAPI.logError("output is not a block");
        }
    }

    @ZenMethod
    public static void add(IItemStack blockOutput, IOreDictEntry oreDictBlock, int time) {
        IBlockState output = null;
        if (InputHelper.isABlock(blockOutput)) {
            output = Block.getBlockFromItem(InputHelper.toStack(blockOutput).getItem()).getDefaultState();

            RecipePureDaisy recipe = new RecipePureDaisy(oreDictBlock.getName(), output, time);
            MineTweakerAPI.apply(new Add(Collections.singletonList(recipe)));
        } else {
            MineTweakerAPI.logError("output is not a block");
        }
    }

    @ZenMethod
    public static void remove(IItemStack output) {
        RecipePureDaisy recipeToRemove = null;
        for (RecipePureDaisy recipe : BotaniaAPI.pureDaisyRecipes) {
            if (recipe.getOutputState().equals(Block.getBlockFromItem(InputHelper.toStack(output).getItem()).getDefaultState())) {
                recipeToRemove = recipe;
                break;
            }
        }
        if (recipeToRemove != null) {
            MineTweakerAPI.apply(new Remove(Collections.singletonList(recipeToRemove)));
        }
    }

    private static class Add extends BaseListAddition<RecipePureDaisy> {

        private HashMap<RecipePureDaisy, PureDaisyRecipeWrapper> recipeJEI = new HashMap<>();

        protected Add(List<RecipePureDaisy> list) {
            super("Pure Daisy", list);
            for (RecipePureDaisy recipe : list) {
                recipeJEI.put(recipe, new PureDaisyRecipeWrapper(recipe));
                BotaniaAPI.pureDaisyRecipes.add(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }

        @Override
        protected String getRecipeInfo(RecipePureDaisy recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(RecipePureDaisy recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (RecipePureDaisy recipe : recipeJEI.keySet()) {
                BotaniaAPI.pureDaisyRecipes.remove(recipe);
                JEIHelper.removeRecipe(recipeJEI.get(recipe), category);
            }
        }
    }

    private static class Remove extends BaseListRemoval<RecipePureDaisy> {

        private HashMap<RecipePureDaisy, PureDaisyRecipeWrapper> recipeJEI = new HashMap<>();

        protected Remove(List<RecipePureDaisy> list) {
            super("Pure Daisy", list);
            for (RecipePureDaisy recipe : list) {

                for (PureDaisyRecipeWrapper recipeJEI : JEIHelper.<PureDaisyRecipeWrapper>getRecipeWrappers(category)) {
                    Ingredients ingredients = new Ingredients();
                    recipeJEI.getIngredients(ingredients);

                    for (List<ItemStack> stacks : ingredients.getOutputs(ItemStack.class)) {
                        for (ItemStack stack : stacks) {
                            if (recipe.getOutputState().equals(Block.getBlockFromItem(stack.getItem()).getDefaultState())) {
                                this.recipeJEI.put(recipe, recipeJEI);
                                BotaniaAPI.pureDaisyRecipes.remove(recipe);
                                JEIHelper.removeRecipe(recipeJEI, category);
                                break;
                            }
                        }
                    }
                }
            }
        }

        @Override
        protected String getRecipeInfo(RecipePureDaisy recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(RecipePureDaisy recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (RecipePureDaisy recipe : recipeJEI.keySet()) {
                BotaniaAPI.pureDaisyRecipes.remove(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }
    }
}
