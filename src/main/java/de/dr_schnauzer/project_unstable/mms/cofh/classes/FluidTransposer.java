package de.dr_schnauzer.project_unstable.mms.cofh.classes;

import cofh.thermalexpansion.plugins.jei.crafting.transposer.TransposerRecipeWrapper;
import cofh.thermalexpansion.util.managers.machine.TransposerManager;
import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.helpers.LogHelper;
import com.blamejared.mtlib.helpers.StackHelper;
import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;
import de.dr_schnauzer.project_unstable.mms.util.JEIHelper;
import mezz.jei.Internal;
import mezz.jei.ingredients.Ingredients;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import minetweaker.api.liquid.ILiquidStack;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fluids.FluidStack;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@ZenClass("mods.thermalexpansion.FluidTransposer")
public class FluidTransposer {

    private static final String category_fill = "thermalexpansion.transposer_fill";
    private static final String category_extract = "thermalexpansion.transposer_extract";

    @ZenMethod
    public static void addFill(IItemStack output, IIngredient input, ILiquidStack liquid, int energy, boolean reversible) {
        List<TransposerManager.TransposerRecipe> recipes = new ArrayList<>();
        for (IItemStack stack : input.getItems()) {
            TransposerManager.TransposerRecipe recipe = TransposerManager.addFillRecipe(
                    energy,
                    InputHelper.toStack(stack),
                    InputHelper.toStack(output),
                    InputHelper.toFluid(liquid),
                    reversible
            );
            if (recipe == null) {
                LogHelper.logError("Fluid Transposer recipe for Input <" + stack.getDisplayName() + "> already exists");
            }
            recipes.add(recipe);
        }
        if (!recipes.isEmpty()) {
            MineTweakerAPI.apply(new Add(recipes, true));
        }
    }

    @ZenMethod
    public static void removeFillByOutput(IIngredient output) {
        List<TransposerManager.TransposerRecipe> recipesToRemove = new ArrayList<>();
        for (TransposerManager.TransposerRecipe recipe : TransposerManager.getFillRecipeList()) {
            for (IItemStack stack : output.getItems()) {
                if (StackHelper.areEqual(recipe.getOutput(), InputHelper.toStack(stack))) {
                    recipesToRemove.add(recipe);
                }
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove, true));
        } else {
            LogHelper.logError("no matching Fluid Transposer recipe found for output " + output);
        }
    }

    @ZenMethod
    public static void removeFillByInput(IIngredient input) {
        List<TransposerManager.TransposerRecipe> recipesToRemove = new ArrayList<>();
        for (TransposerManager.TransposerRecipe recipe : TransposerManager.getFillRecipeList()) {
            for (IItemStack stack : input.getItems()) {
                if (StackHelper.areEqual(recipe.getInput(), InputHelper.toStack(stack))) {
                    recipesToRemove.add(recipe);
                }
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove, true));
        } else {
            LogHelper.logError("no matching Fluid Transposer recipe found for input " + input);
        }
    }

    @ZenMethod
    public static void addExtract(IItemStack output, IIngredient input, ILiquidStack liquid, int energy, int chance, boolean reversible) {
        List<TransposerManager.TransposerRecipe> recipes = new ArrayList<>();
        for (IItemStack stack : input.getItems()) {
            TransposerManager.TransposerRecipe recipe = TransposerManager.addExtractRecipe(
                    energy,
                    InputHelper.toStack(stack),
                    InputHelper.toStack(output),
                    InputHelper.toFluid(liquid),
                    chance,
                    reversible
            );
            if (recipe == null) {
                LogHelper.logError("Fluid Transposer recipe for Input <" + stack.getDisplayName() + "> already exists");
            }
            recipes.add(recipe);
        }
        if (!recipes.isEmpty()) {
            MineTweakerAPI.apply(new Add(recipes, false));
        }
    }

    @ZenMethod
    public static void removeExtractByOutput(IIngredient output) {
        List<TransposerManager.TransposerRecipe> recipesToRemove = new ArrayList<>();
        for (TransposerManager.TransposerRecipe recipe : TransposerManager.getExtractRecipeList()) {
            for (IItemStack stack : output.getItems()) {
                if (StackHelper.areEqual(recipe.getOutput(), InputHelper.toStack(stack))) {
                    recipesToRemove.add(recipe);
                }
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove, false));
        } else {
            LogHelper.logError("no matching Fluid Transposer recipe found for output " + output);
        }
    }

    @ZenMethod
    public static void removeExtractByInput(IIngredient input, ILiquidStack liquid) {
        List<TransposerManager.TransposerRecipe> recipesToRemove = new ArrayList<>();
        for (TransposerManager.TransposerRecipe recipe : TransposerManager.getExtractRecipeList()) {
            for (IItemStack stack : input.getItems()) {
                if (StackHelper.areEqual(recipe.getInput(), InputHelper.toStack(stack))) {
                    for (ILiquidStack liquidStack : liquid.getLiquids()) {
                        if (StackHelper.areEqual(InputHelper.toFluid(liquidStack), recipe.getFluid())) {
                            recipesToRemove.add(recipe);
                        }
                    }
                }
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove, false));
        } else {
            LogHelper.logError("no matching Fluid Transposer recipe found for input " + input);
        }
    }

    private static class Add extends BaseListAddition<TransposerManager.TransposerRecipe> {

        private HashMap<TransposerManager.TransposerRecipe, TransposerRecipeWrapper> recipeJEI = new HashMap<>();
        private boolean fill;

        protected Add(List<TransposerManager.TransposerRecipe> list, boolean fill) {
            super("Redstone Furnace", list);
            this.fill = fill;

            if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
                for (TransposerManager.TransposerRecipe recipe : list) {
                    recipeJEI.put(recipe, new TransposerRecipeWrapper(Internal.getHelpers().getGuiHelper(), recipe, fill ? category_fill : category_extract));
                    //already registered recipe ... TransposerManager.getRecipeList();
                    JEIHelper.addRecipe(recipeJEI.get(recipe), fill ? category_fill : category_extract);
                }
            }
        }

        @Override
        protected String getRecipeInfo(TransposerManager.TransposerRecipe recipe) {
            return recipe.getOutput().toString();
        }

        @Override
        public String getJEICategory(TransposerManager.TransposerRecipe recipe) {
            return fill ? category_fill : category_extract;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (TransposerManager.TransposerRecipe recipe : recipeJEI.keySet()) {
                if (fill) {
                    TransposerManager.removeFillRecipe(recipe.getInput(), recipe.getFluid());
                    JEIHelper.removeRecipe(recipeJEI.get(recipe), category_fill);
                } else {
                    TransposerManager.removeFillRecipe(recipe.getInput(), recipe.getFluid());
                    JEIHelper.removeRecipe(recipeJEI.get(recipe), category_extract);
                }
            }
        }
    }

    private static class Remove extends BaseListRemoval<TransposerManager.TransposerRecipe> {

        private HashMap<TransposerManager.TransposerRecipe, TransposerRecipeWrapper> recipeJEI = new HashMap<>();
        private boolean fill;

        protected Remove(List<TransposerManager.TransposerRecipe> list, boolean fill) {
            super("Redstone Furnace", list);
            this.fill = fill;
            for (TransposerManager.TransposerRecipe recipe : list) {

                for (TransposerRecipeWrapper recipeJEI : JEIHelper.<TransposerRecipeWrapper>getRecipeWrappers(fill ? category_fill : category_extract)) {
                    Ingredients ingredients = new Ingredients();
                    recipeJEI.getIngredients(ingredients);

                    boolean flag = false;

                    for (List<FluidStack> fluids : ingredients.getInputs(FluidStack.class)) {
                        for (FluidStack fluid : fluids) {
                            if (StackHelper.areEqual(fluid, recipe.getFluid())) {
                                flag = true;
                                break;
                            }
                        }
                    }
                    if(!flag) continue;
                    flag = false;
                    for (List<ItemStack> stacks : ingredients.getInputs(ItemStack.class)) {
                        for (ItemStack stack : stacks) {
                            if (StackHelper.areEqual(stack, recipe.getInput())) {
                                flag = true;
                                break;
                            }
                        }
                    }
                    if (flag) {
                        flag = false;
                        for (List<ItemStack> stacks : ingredients.getOutputs(ItemStack.class)) {
                            for (ItemStack stack : stacks) {
                                if (StackHelper.areEqual(stack, recipe.getOutput())) {
                                    flag = true;
                                    break;
                                }
                            }
                        }
                        if (flag) {
                            this.recipeJEI.put(recipe, recipeJEI);
                            if (fill) {
                                TransposerManager.removeFillRecipe(recipe.getInput(), recipe.getFluid());
                                JEIHelper.removeRecipe(recipeJEI, category_fill);
                            } else {
                                TransposerManager.removeExtractRecipe(recipe.getInput());
                                JEIHelper.removeRecipe(recipeJEI, category_extract);
                            }
                            break;
                        }
                    }

                }
            }
        }

        @Override
        protected String getRecipeInfo(TransposerManager.TransposerRecipe recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(TransposerManager.TransposerRecipe recipe) {
            return fill ? category_fill : category_extract;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (TransposerManager.TransposerRecipe recipe : recipeJEI.keySet()) {
                if (fill) {
                    TransposerManager.addFillRecipe(recipe.getEnergy(), recipe.getInput(), recipe.getOutput(), recipe.getFluid(), false);
                    JEIHelper.removeRecipe(recipeJEI.get(recipe), category_fill);
                } else {
                    TransposerManager.addExtractRecipe(recipe.getEnergy(), recipe.getInput(), recipe.getOutput(), recipe.getFluid(), recipe.getChance(), false);
                    JEIHelper.removeRecipe(recipeJEI.get(recipe), category_extract);
                }
            }
        }
    }
}
