package de.dr_schnauzer.project_unstable.mms.cofh.classes;

import cofh.thermalexpansion.plugins.jei.crafting.pulverizer.PulverizerRecipeWrapper;
import cofh.thermalexpansion.util.managers.machine.PulverizerManager;
import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.helpers.LogHelper;
import com.blamejared.mtlib.helpers.StackHelper;
import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;
import de.dr_schnauzer.project_unstable.mms.util.JEIHelper;
import mezz.jei.Internal;
import mezz.jei.ingredients.Ingredients;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import stanhebben.zenscript.annotations.Optional;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@ZenClass("mods.thermalexpansion.Pulverizer")
public class Pulverizer {

    private static final String category = "thermalexpansion.pulverizer";

    @ZenMethod
    public static void add(IItemStack output, @Optional IItemStack secondaryOutput, IIngredient input, int secondaryChance, int energy) {
        List<PulverizerManager.PulverizerRecipe> recipes = new ArrayList<>();
        for (IItemStack stack : input.getItems()) {
            PulverizerManager.PulverizerRecipe recipe = PulverizerManager.addRecipe(
                    energy,
                    InputHelper.toStack(stack),
                    InputHelper.toStack(output),
                    InputHelper.toStack(secondaryOutput),
                    secondaryChance
            );
            if (recipe == null) {
                LogHelper.logError("Pulverizer recipe for Input <" + stack.getDisplayName() + "> already exists");
            }
            recipes.add(recipe);
        }
        if (!recipes.isEmpty()) {
            MineTweakerAPI.apply(new Pulverizer.Add(recipes));
        } else {
            LogHelper.logError("no matching recipe found");
        }
    }

    @ZenMethod
    public static void removeByOutput(IItemStack output, @Optional IItemStack secondaryOutput) {
        List<PulverizerManager.PulverizerRecipe> recipesToRemove = new ArrayList<>();
        for (PulverizerManager.PulverizerRecipe recipe : PulverizerManager.getRecipeList()) {
            if (StackHelper.areEqual(recipe.getPrimaryOutput(), InputHelper.toStack(output))) {
                if (ItemStack.areItemStacksEqual(recipe.getSecondaryOutput(), InputHelper.toStack(secondaryOutput))) {
                    recipesToRemove.add(recipe);
                }
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove));
        }
    }

    @ZenMethod
    public static void removeByInput(IIngredient input) {
        List<PulverizerManager.PulverizerRecipe> recipesToRemove = new ArrayList<>();
        for (PulverizerManager.PulverizerRecipe recipe : PulverizerManager.getRecipeList()) {
            for (IItemStack stack : input.getItems()) {
                if (StackHelper.areEqual(recipe.getInput(), InputHelper.toStack(stack))) {
                    recipesToRemove.add(recipe);
                }
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove));
        }
    }

    private static class Add extends BaseListAddition<PulverizerManager.PulverizerRecipe> {

        private HashMap<PulverizerManager.PulverizerRecipe, PulverizerRecipeWrapper> recipeJEI = new HashMap<>();

        protected Add(List<PulverizerManager.PulverizerRecipe> list) {
            super("Pulverizer", list);
            if (FMLCommonHandler.instance().getEffectiveSide() == Side.CLIENT) {
                for (PulverizerManager.PulverizerRecipe recipe : list) {
                    recipeJEI.put(recipe, new PulverizerRecipeWrapper(Internal.getHelpers().getGuiHelper(), recipe));
                    //already registered recipe ... PulverizerManager.getRecipeList();
                    JEIHelper.addRecipe(recipeJEI.get(recipe), category);
                }
            }
        }

        @Override
        protected String getRecipeInfo(PulverizerManager.PulverizerRecipe recipe) {
            return recipe.getPrimaryOutput().toString() + " + " + recipe.getSecondaryOutput().toString();
        }

        @Override
        public String getJEICategory(PulverizerManager.PulverizerRecipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (PulverizerManager.PulverizerRecipe recipe : recipeJEI.keySet()) {
                PulverizerManager.removeRecipe(recipe.getInput());
                JEIHelper.removeRecipe(recipeJEI.get(recipe), category);
            }
        }
    }

    private static class Remove extends BaseListRemoval<PulverizerManager.PulverizerRecipe> {

        private HashMap<PulverizerManager.PulverizerRecipe, PulverizerRecipeWrapper> recipeJEI = new HashMap<>();

        protected Remove(List<PulverizerManager.PulverizerRecipe> list) {
            super("Pulverizer", list);
            for (PulverizerManager.PulverizerRecipe recipe : list) {

                for (PulverizerRecipeWrapper recipeJEI : JEIHelper.<PulverizerRecipeWrapper>getRecipeWrappers(category)) {
                    Ingredients ingredients = new Ingredients();
                    recipeJEI.getIngredients(ingredients);

                    boolean flag = false;
                    for (List<ItemStack> stacks : ingredients.getInputs(ItemStack.class)) {
                        for (ItemStack stack : stacks) {
                            if (StackHelper.areEqual(stack, recipe.getInput())) {
                                flag = true;
                                break;
                            }
                        }
                    }
                    if (flag) {
                        List<List<ItemStack>> stacks = ingredients.getOutputs(ItemStack.class);
                        flag = false;
                        for (ItemStack primary : stacks.get(0)) {
                            if (StackHelper.areEqual(recipe.getPrimaryOutput(), primary)) {
                                flag = true;
                                break;
                            }
                        }
                        if (!flag) continue;
                        flag = false;
                        if (stacks.get(1).isEmpty()) {
                            flag = !recipe.getSecondaryOutput().func_190926_b();
                        } else {
                            for (ItemStack secondary : stacks.get(1)) {
                                if (ItemStack.areItemStacksEqual(recipe.getSecondaryOutput(), secondary)) {
                                    flag = true;
                                    break;
                                }
                            }
                        }
                        this.recipeJEI.put(recipe, recipeJEI);
                        PulverizerManager.removeRecipe(recipe.getInput());
                        JEIHelper.removeRecipe(recipeJEI, category);
                        break;
                    }

                }
            }
        }

        @Override
        protected String getRecipeInfo(PulverizerManager.PulverizerRecipe recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(PulverizerManager.PulverizerRecipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (PulverizerManager.PulverizerRecipe recipe : recipeJEI.keySet()) {
                PulverizerManager.addRecipe(recipe.getEnergy(), recipe.getInput(), recipe.getPrimaryOutput(), recipe.getSecondaryOutput(), recipe.getSecondaryOutputChance());
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }
    }
}
