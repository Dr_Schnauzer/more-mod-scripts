package de.dr_schnauzer.project_unstable.mms.bloodmagic.classes;

import WayofTime.bloodmagic.api.recipe.TartaricForgeRecipe;
import WayofTime.bloodmagic.api.registry.TartaricForgeRecipeRegistry;
import WayofTime.bloodmagic.compat.jei.forge.TartaricForgeRecipeJEI;
import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.helpers.StackHelper;
import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;
import de.dr_schnauzer.project_unstable.mms.util.JEIHelper;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@ZenClass("mods.bloodmagic.Forge")
public class HellfireForge {

    private static final String category = "bloodmagic:soulForge";

    @ZenMethod
    public static void add(IItemStack output, double minSouls, double drain, IIngredient[] input) {
        TartaricForgeRecipe recipe = new TartaricForgeRecipe(InputHelper.toStack(output), minSouls, drain, InputHelper.toObjects(input));
        MineTweakerAPI.apply(new Add(Collections.singletonList(recipe)));
    }

    @ZenMethod
    public static void remove(IItemStack output) {
        List<TartaricForgeRecipe> recipeToRemove = new ArrayList<>();
        for (TartaricForgeRecipe recipe : TartaricForgeRecipeRegistry.getRecipeList()) {
            if (StackHelper.areEqual(recipe.getRecipeOutput(), InputHelper.toStack(output))) {
                recipeToRemove.add(recipe);
            }
        }
        if (!recipeToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipeToRemove));
        }
    }

    private static class Add extends BaseListAddition<TartaricForgeRecipe> {

        private HashMap<TartaricForgeRecipe, TartaricForgeRecipeJEI> recipeJEI = new HashMap<>();

        protected Add(List<TartaricForgeRecipe> list) {
            super("Hellfire Forge", list);
            for (TartaricForgeRecipe recipe : list) {
                recipeJEI.put(recipe, new TartaricForgeRecipeJEI(recipe));
                TartaricForgeRecipeRegistry.registerRecipe(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }

        @Override
        protected String getRecipeInfo(TartaricForgeRecipe recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(TartaricForgeRecipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (TartaricForgeRecipe recipe : recipeJEI.keySet()) {
                TartaricForgeRecipeRegistry.removeRecipe(recipe);
                JEIHelper.removeRecipe(recipeJEI.get(recipe), category);
            }
        }
    }

    private static class Remove extends BaseListRemoval<TartaricForgeRecipe> {

        private HashMap<TartaricForgeRecipe, TartaricForgeRecipeJEI> recipeJEI = new HashMap<>();

        @SuppressWarnings("unchecked")
        protected Remove(List<TartaricForgeRecipe> list) {
            super("Hellfire Forge", list);
            for (TartaricForgeRecipe recipe : list) {

                for (TartaricForgeRecipeJEI recipeJEI : JEIHelper.<TartaricForgeRecipeJEI>getRecipeWrappers(category)) {
                    if (recipeJEI.getRecipe().equals(recipe)) {
                        this.recipeJEI.put(recipe, recipeJEI);
                        TartaricForgeRecipeRegistry.removeRecipe(recipe);
                        JEIHelper.removeRecipe(recipeJEI, category);
                    }
                }
            }
        }

        @Override
        protected String getRecipeInfo(TartaricForgeRecipe recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(TartaricForgeRecipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (TartaricForgeRecipe recipe : recipeJEI.keySet()) {
                TartaricForgeRecipeRegistry.registerRecipe(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }
    }
}
