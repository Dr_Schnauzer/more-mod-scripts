package de.dr_schnauzer.project_unstable.mms.botania.classes;

import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.helpers.StackHelper;
import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;
import de.dr_schnauzer.project_unstable.mms.util.JEIHelper;
import mezz.jei.ingredients.Ingredients;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;
import vazkii.botania.api.BotaniaAPI;
import vazkii.botania.api.recipe.RecipeRuneAltar;
import vazkii.botania.client.integration.jei.runicaltar.RunicAltarRecipeWrapper;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@ZenClass("mods.botania.RunicAltar")
public class RunicAltar {

    private static final String category = "botania.runicAltar";

    @ZenMethod
    public static void add(IItemStack output, IIngredient[] input, int mana) {
        RecipeRuneAltar recipe = new RecipeRuneAltar(InputHelper.toStack(output), mana, InputHelper.toObjects(input));
        MineTweakerAPI.apply(new Add(Collections.singletonList(recipe)));
    }

    @ZenMethod
    public static void remove(IItemStack output) {
        RecipeRuneAltar recipeToRemove = null;
        for (RecipeRuneAltar recipe : BotaniaAPI.runeAltarRecipes) {
            if (StackHelper.areEqual(recipe.getOutput(), InputHelper.toStack(output))) {
                recipeToRemove = recipe;
                break;
            }
        }
        if (recipeToRemove != null) {
            MineTweakerAPI.apply(new Remove(Collections.singletonList(recipeToRemove)));
        }
    }

    private static class Add extends BaseListAddition<RecipeRuneAltar> {

        private HashMap<RecipeRuneAltar, RunicAltarRecipeWrapper> recipeJEI = new HashMap<>();

        protected Add(List<RecipeRuneAltar> list) {
            super("Runic Altar", list);
            for (RecipeRuneAltar recipe : list) {
                recipeJEI.put(recipe, new RunicAltarRecipeWrapper(recipe));
                BotaniaAPI.runeAltarRecipes.add(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }

        @Override
        protected String getRecipeInfo(RecipeRuneAltar recipe) {
            return recipe.getOutput().toString();
        }

        @Override
        public String getJEICategory(RecipeRuneAltar recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (RecipeRuneAltar recipe : recipeJEI.keySet()) {
                BotaniaAPI.runeAltarRecipes.remove(recipe);
                JEIHelper.removeRecipe(recipeJEI.get(recipe), category);
            }
        }
    }

    private static class Remove extends BaseListRemoval<RecipeRuneAltar> {

        private HashMap<RecipeRuneAltar, RunicAltarRecipeWrapper> recipeJEI = new HashMap<>();

        protected Remove(List<RecipeRuneAltar> list) {
            super("Runic Altar", list);
            for (RecipeRuneAltar recipe : list) {

                for (RunicAltarRecipeWrapper recipeJEI : JEIHelper.<RunicAltarRecipeWrapper>getRecipeWrappers(category)) {
                    Ingredients ingredients = new Ingredients();
                    recipeJEI.getIngredients(ingredients);

                    for (List<ItemStack> stacks : ingredients.getOutputs(ItemStack.class)) {
                        for (ItemStack stack : stacks) {
                            if (StackHelper.areEqual(stack, recipe.getOutput())) {
                                this.recipeJEI.put(recipe, recipeJEI);
                                BotaniaAPI.runeAltarRecipes.remove(recipe);
                                JEIHelper.removeRecipe(recipeJEI, category);
                                break;
                            }
                        }
                    }
                }
            }
        }

        @Override
        protected String getRecipeInfo(RecipeRuneAltar recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(RecipeRuneAltar recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (RecipeRuneAltar recipe : recipeJEI.keySet()) {
                BotaniaAPI.runeAltarRecipes.remove(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }
    }
}
