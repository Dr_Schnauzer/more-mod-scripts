#More Mod Scripts

More Mod Scripts is an Addon for Crafttweaker ([link](https://minecraft.curseforge.com/projects/crafttweaker?gameCategorySlug=mc-mods&projectID=239197)) and adds ZenScript Support to many mods.


This mod was created to add the missing ZenScript integrations to mods, which are not covered by ModTweaker ([link](https://minecraft.curseforge.com/projects/modtweaker?gameCategorySlug=mc-mods&projectID=220954)). This mod is not perfect or even close to it. The Mod was initially just created for the Modpack my friends and I are creating. Therefore some features may be missing and the source code is probably not the cleanest. Due to that, the mod requires JEI to be installed, but every change to the supported recipes will (hopefully) always be displayed in JEI and fully support the Undo function.


__Again: I don't want to replace ModTweaker and was originally only created for Minecraft 1.11.2. If I can spare the time, I will port the mod to higher versions.__