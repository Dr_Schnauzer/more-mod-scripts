package de.dr_schnauzer.project_unstable.mms.botania;

import de.dr_schnauzer.project_unstable.mms.botania.classes.*;
import minetweaker.MineTweakerAPI;

public class Botania {

    public static void register() {

        MineTweakerAPI.registerClass(PetalApothecary.class);
        MineTweakerAPI.registerClass(RunicAltar.class);
        MineTweakerAPI.registerClass(ElvenTrade.class);
        MineTweakerAPI.registerClass(PureDaisy.class);
        MineTweakerAPI.registerClass(ManaInfusion.class);
    }
}
