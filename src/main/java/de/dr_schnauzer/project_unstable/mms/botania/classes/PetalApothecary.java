package de.dr_schnauzer.project_unstable.mms.botania.classes;

import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.helpers.StackHelper;
import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;
import de.dr_schnauzer.project_unstable.mms.util.JEIHelper;
import mezz.jei.ingredients.Ingredients;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;
import vazkii.botania.api.BotaniaAPI;
import vazkii.botania.api.recipe.RecipePetals;
import vazkii.botania.client.integration.jei.petalapothecary.PetalApothecaryRecipeWrapper;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@ZenClass("mods.botania.Apothecary")
public class PetalApothecary {

    private static final String category = "botania.petals";

    @ZenMethod
    public static void add(IItemStack output, IIngredient[] input) {
        RecipePetals recipe = new RecipePetals(InputHelper.toStack(output), InputHelper.toObjects(input));
        MineTweakerAPI.apply(new Add(Collections.singletonList(recipe)));
    }

    @ZenMethod
    public static void remove(IItemStack output) {
        RecipePetals recipeToRemove = null;
        for (RecipePetals recipe : BotaniaAPI.petalRecipes) {
            if (StackHelper.areEqual(recipe.getOutput(), InputHelper.toStack(output))) {
                recipeToRemove = recipe;
                break;
            }
        }
        if (recipeToRemove != null) {
            MineTweakerAPI.apply(new Remove(Collections.singletonList(recipeToRemove)));
        }
    }

    private static class Add extends BaseListAddition<RecipePetals> {

        private HashMap<RecipePetals, PetalApothecaryRecipeWrapper> recipeJEI = new HashMap<>();

        protected Add(List<RecipePetals> list) {
            super("Petal Apothecary", list);
            for (RecipePetals recipe : list) {
                recipeJEI.put(recipe, new PetalApothecaryRecipeWrapper(recipe));
                BotaniaAPI.petalRecipes.add(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }

        @Override
        protected String getRecipeInfo(RecipePetals recipe) {
            return recipe.getOutput().toString();
        }

        @Override
        public String getJEICategory(RecipePetals recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (RecipePetals recipe : recipeJEI.keySet()) {
                BotaniaAPI.petalRecipes.remove(recipe);
                JEIHelper.removeRecipe(recipeJEI.get(recipe), category);
            }
        }
    }

    private static class Remove extends BaseListRemoval<RecipePetals> {

        private HashMap<RecipePetals, PetalApothecaryRecipeWrapper> recipeJEI = new HashMap<>();

        protected Remove(List<RecipePetals> list) {
            super("Petal Apothecary", list);
            for (RecipePetals recipe : list) {

                for (PetalApothecaryRecipeWrapper recipeJEI : JEIHelper.<PetalApothecaryRecipeWrapper>getRecipeWrappers(category)) {
                    Ingredients ingredients = new Ingredients();
                    recipeJEI.getIngredients(ingredients);

                    for (List<ItemStack> stacks : ingredients.getOutputs(ItemStack.class)) {
                        for (ItemStack stack : stacks) {
                            if (StackHelper.areEqual(stack, recipe.getOutput())) {
                                this.recipeJEI.put(recipe, recipeJEI);
                                BotaniaAPI.petalRecipes.remove(recipe);
                                JEIHelper.removeRecipe(recipeJEI, category);
                                break;
                            }
                        }
                    }
                }
            }
        }

        @Override
        protected String getRecipeInfo(RecipePetals recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(RecipePetals recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (RecipePetals recipe : recipeJEI.keySet()) {
                BotaniaAPI.petalRecipes.remove(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }
    }
}
