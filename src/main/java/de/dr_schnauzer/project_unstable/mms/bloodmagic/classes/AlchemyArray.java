package de.dr_schnauzer.project_unstable.mms.bloodmagic.classes;

import WayofTime.bloodmagic.api.registry.AlchemyArrayRecipeRegistry;
import WayofTime.bloodmagic.compat.jei.alchemyArray.AlchemyArrayCraftingRecipeJEI;
import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;
import de.dr_schnauzer.project_unstable.mms.util.JEIHelper;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@ZenClass("mods.bloodmagic.Forge")
public class AlchemyArray {

    /*
    TODO
    public static final String category_fill = "bloodmagic:soulForge";

    @ZenMethod
    public static void add(IItemStack output, IIngredient[] input) {
        AlchemyArrayRecipeRegistry.AlchemyArrayRecipe recipe = new AlchemyArrayRecipeRegistry.AlchemyArrayRecipe(InputHelper.toStack(output), minSouls, drain, InputHelper.toObjects(input));
        MineTweakerAPI.apply(new Add(Collections.singletonList(recipe)));
    }

    @ZenMethod
    public static void remove(IItemStack output) {
        List<AlchemyArrayRecipeRegistry.AlchemyArrayRecipe> recipeToRemove = new ArrayList<>();
        for (AlchemyArrayRecipeRegistry.AlchemyArrayRecipe recipe : AlchemyArrayRecipeRegistry.getRecipeList()) {
            if (ItemStack.areItemStacksEqual(recipe.getRecipeOutput(), InputHelper.toStack(output))) {
                recipeToRemove.add(recipe);
            }
        }
        if (!recipeToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipeToRemove));
        }
    }

    private static class Add extends BaseListAddition<AlchemyArrayRecipeRegistry.AlchemyArrayRecipe> {

        private HashMap<AlchemyArrayRecipeRegistry.AlchemyArrayRecipe, AlchemyArrayCraftingRecipeJEI> recipeJEI = new HashMap<>();

        protected Add(List<AlchemyArrayRecipeRegistry.AlchemyArrayRecipe> list) {
            super("Hellfire Forge", list);
            for (AlchemyArrayRecipeRegistry.AlchemyArrayRecipe recipe : list) {
                recipeJEI.put(recipe, new AlchemyArrayCraftingRecipeJEI(recipe));
                AlchemyArrayRecipeRegistry.registerRecipe(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category_fill);
            }
        }

        @Override
        protected String getRecipeInfo(AlchemyArrayRecipeRegistry.AlchemyArrayRecipe recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(AlchemyArrayRecipeRegistry.AlchemyArrayRecipe recipe) {
            return category_fill;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (AlchemyArrayRecipeRegistry.AlchemyArrayRecipe recipe : list) {
                AlchemyArrayRecipeRegistry.removeRecipe(recipe);
                JEIHelper.removeRecipe(recipeJEI.get(recipe), category_fill);
            }
        }
    }

    private static class Remove extends BaseListRemoval<AlchemyArrayRecipeRegistry.AlchemyArrayRecipe> {

        private HashMap<AlchemyArrayRecipeRegistry.AlchemyArrayRecipe, AlchemyArrayCraftingRecipeJEI> recipeJEI = new HashMap<>();

        @SuppressWarnings("unchecked")
        protected Remove(List<AlchemyArrayRecipeRegistry.AlchemyArrayRecipe> list) {
            super("Hellfire Forge", list);
            for (AlchemyArrayRecipeRegistry.AlchemyArrayRecipe recipe : list) {

                for (AlchemyArrayCraftingRecipeJEI recipeJEI : JEIHelper.<AlchemyArrayCraftingRecipeJEI>getRecipeWrappers(category_fill)) {
                    if (recipeJEI.getRecipe().equals(recipe)) {
                        this.recipeJEI.put(recipe, recipeJEI);
                        AlchemyArrayRecipeRegistry.removeRecipe(recipe);
                        JEIHelper.removeRecipe(recipeJEI, category_fill);
                    }
                }
            }
        }

        @Override
        protected String getRecipeInfo(AlchemyArrayRecipeRegistry.AlchemyArrayRecipe recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(AlchemyArrayRecipeRegistry.AlchemyArrayRecipe recipe) {
            return category_fill;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (AlchemyArrayRecipeRegistry.AlchemyArrayRecipe recipe : list) {
                AlchemyArrayRecipeRegistry.registerRecipe(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category_fill);
            }
        }
    }
    */
}
