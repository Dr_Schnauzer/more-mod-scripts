package de.dr_schnauzer.project_unstable.mms.bloodmagic.classes;

import WayofTime.bloodmagic.api.ItemStackWrapper;
import WayofTime.bloodmagic.api.altar.EnumAltarTier;
import WayofTime.bloodmagic.api.registry.AltarRecipeRegistry;
import WayofTime.bloodmagic.compat.jei.altar.AltarRecipeCategory;
import WayofTime.bloodmagic.compat.jei.altar.AltarRecipeJEI;
import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.helpers.StackHelper;
import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;
import de.dr_schnauzer.project_unstable.mms.util.JEIHelper;
import mezz.jei.ingredients.Ingredients;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.Optional;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@ZenClass("mods.bloodmagic.Altar")
public class Altar {

    private static final String category = "bloodmagic:altar";

    @ZenMethod
    public static void add(IItemStack output, IIngredient input, int minTier, int lpDrained, int consumeRate, int drainRate) {
        add(output, input, minTier, lpDrained, consumeRate, drainRate, false);
    }

        @ZenMethod
    public static void add(IItemStack output, IIngredient input, int minTier, int lpDrained, int consumeRate, int drainRate, boolean fillable) {
        if (minTier > 0 && minTier < EnumAltarTier.MAXTIERS) {
            EnumAltarTier tier = EnumAltarTier.values()[minTier-1];

            List<ItemStack> stacks = new ArrayList<>();
            for (IItemStack stack : input.getItems()) {
                stacks.add(InputHelper.toStack(stack));
            }

            AltarRecipeRegistry.AltarRecipe recipe = new AltarRecipeRegistry.AltarRecipe(stacks, InputHelper.toStack(output), tier, lpDrained, consumeRate, drainRate, fillable);
            MineTweakerAPI.apply(new Add(Collections.singletonList(recipe)));
        }
    }

    @ZenMethod
    public static void remove(IItemStack output) {
        List<AltarRecipeRegistry.AltarRecipe> recipeToRemove = new ArrayList<>();
        for (AltarRecipeRegistry.AltarRecipe recipe : AltarRecipeRegistry.getRecipes().values()) {
            if (StackHelper.areEqual(recipe.getOutput(), InputHelper.toStack(output))) {
                recipeToRemove.add(recipe);
            }
        }
        if (!recipeToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipeToRemove));
        }
    }

    private static class Add extends BaseListAddition<AltarRecipeRegistry.AltarRecipe> {

        private HashMap<AltarRecipeRegistry.AltarRecipe, AltarRecipeJEI> recipeJEI = new HashMap<>();

        protected Add(List<AltarRecipeRegistry.AltarRecipe> list) {
            super("Altar", list);
            for (AltarRecipeRegistry.AltarRecipe recipe : list) {
                recipeJEI.put(recipe, new AltarRecipeJEI(ItemStackWrapper.toStackList(recipe.getInput()), recipe.getOutput(), recipe.getMinTier().toInt(), recipe.getSyphon(), recipe.getConsumeRate(), recipe.getDrainRate()));
                AltarRecipeRegistry.registerRecipe(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }

        @Override
        protected String getRecipeInfo(AltarRecipeRegistry.AltarRecipe recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(AltarRecipeRegistry.AltarRecipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (AltarRecipeRegistry.AltarRecipe recipe : recipeJEI.keySet()) {
                AltarRecipeRegistry.removeRecipe(recipe);
                JEIHelper.removeRecipe(recipeJEI.get(recipe), category);
            }
        }
    }

    private static class Remove extends BaseListRemoval<AltarRecipeRegistry.AltarRecipe> {

        private HashMap<AltarRecipeRegistry.AltarRecipe, AltarRecipeJEI> recipeJEI = new HashMap<>();

        @SuppressWarnings("unchecked")
        protected Remove(List<AltarRecipeRegistry.AltarRecipe> list) {
            super("Altar", list);
            for (AltarRecipeRegistry.AltarRecipe recipe : list) {
                for (AltarRecipeJEI recipeJEI : JEIHelper.<AltarRecipeJEI>getRecipeWrappers(category)) {
                    Ingredients ingredients = new Ingredients();
                    recipeJEI.getIngredients(ingredients);

                    boolean flag = false;
                    for (List<ItemStack> stacks : ingredients.getOutputs(ItemStack.class)) {
                        for (ItemStack stack : stacks) {
                            if (StackHelper.areEqual(stack, recipe.getOutput())) {
                                flag = true;
                                break;
                            }
                        }
                    }
                    if (flag) {
                        for (List<ItemStack> stacks : ingredients.getInputs(ItemStack.class)) {
                            for (ItemStack stack : stacks) {
                                for (ItemStack inputStack : ItemStackWrapper.toStackList(recipe.getInput())) {
                                    if (StackHelper.areEqual(stack, inputStack)) {
                                        this.recipeJEI.put(recipe, recipeJEI);
                                        AltarRecipeRegistry.removeRecipe(recipe);
                                        JEIHelper.removeRecipe(recipeJEI, category);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        @Override
        protected String getRecipeInfo(AltarRecipeRegistry.AltarRecipe recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(AltarRecipeRegistry.AltarRecipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (AltarRecipeRegistry.AltarRecipe recipe : recipeJEI.keySet()) {
                AltarRecipeRegistry.registerRecipe(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }
    }
}
