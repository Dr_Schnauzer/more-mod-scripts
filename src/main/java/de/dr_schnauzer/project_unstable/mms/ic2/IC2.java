package de.dr_schnauzer.project_unstable.mms.ic2;

import de.dr_schnauzer.project_unstable.mms.ic2.classes.*;
import minetweaker.MineTweakerAPI;

public class IC2 {

    public static void register() {

        MineTweakerAPI.registerClass(Macerator.class);
        MineTweakerAPI.registerClass(Compressor.class);
        MineTweakerAPI.registerClass(Extractor.class);
        MineTweakerAPI.registerClass(ThermalCentrifuge.class);
        MineTweakerAPI.registerClass(BlastFurnace.class);
        MineTweakerAPI.registerClass(OreWasher.class);
        MineTweakerAPI.registerClass(MetalFormer.class);
        MineTweakerAPI.registerClass(CuttingMachine.class);
    }
}
