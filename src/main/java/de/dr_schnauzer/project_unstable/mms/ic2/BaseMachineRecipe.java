package de.dr_schnauzer.project_unstable.mms.ic2;

import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;
import de.dr_schnauzer.project_unstable.mms.util.ItemHelper;
import de.dr_schnauzer.project_unstable.mms.util.JEIHelper;
import ic2.api.recipe.IBasicMachineRecipeManager;
import ic2.api.recipe.IRecipeInput;
import ic2.api.recipe.MachineRecipe;
import ic2.jeiIntegration.recipe.machine.DynamicCategory;
import ic2.jeiIntegration.recipe.machine.IORecipeCategory;
import ic2.jeiIntegration.recipe.machine.IORecipeWrapper;
import ic2.jeiIntegration.recipe.machine.IRecipeWrapperGenerator;
import mezz.jei.Internal;
import mezz.jei.api.recipe.IRecipeCategory;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.item.ItemStack;

import java.util.*;

public class BaseMachineRecipe {

    public static void add(IC2Recipe recipe, HashMap<IC2Recipe, IORecipeWrapper> recipeJEI, IBasicMachineRecipeManager manager, IRecipeWrapperGenerator<IBasicMachineRecipeManager> generator, String category) {
        manager.addRecipe(recipe.getInput(), recipe.getOutputs(), recipe.getMeta(), false);

        boolean added = false;
        if (Internal.getRuntime() != null && Internal.getRuntime().getRecipeRegistry() != null) {
            List<IRecipeCategory> recipeCategory = Internal.getRuntime().getRecipeRegistry().getRecipeCategories(Collections.singletonList(category));
            if (recipeCategory.get(0) instanceof DynamicCategory) {
                for (IRecipeWrapper wrapper : generator.getRecipeList((IORecipeCategory<IBasicMachineRecipeManager>) recipeCategory.get(0))) {
                    if (wrapper instanceof IORecipeWrapper) {
                        if (ItemHelper.areStacksEqual(((IORecipeWrapper) wrapper).getOutputs(), recipe.getOutputs())) {
                            for (List<ItemStack> inputItems : ((IORecipeWrapper) wrapper).getInputs()) {
                                if (ItemHelper.areStacksEqual(inputItems, recipe.getInput().getInputs())) {
                                    recipeJEI.put(recipe, (IORecipeWrapper) wrapper);
                                    JEIHelper.addRecipe(wrapper, category);
                                    added = true;
                                    break;
                                }
                            }
                        }
                    }
                    if (added) break;
                }
            }
        }
    }

    public static void remove(IC2Recipe recipe, IRecipeWrapper wrapper, IBasicMachineRecipeManager manager, String category) {
        Iterator<? extends MachineRecipe<IRecipeInput, Collection<ItemStack>>> iterator = manager.getRecipes().iterator();
        boolean removed = false;
        while (iterator.hasNext()) {
            MachineRecipe<IRecipeInput, Collection<ItemStack>> machineRecipe = iterator.next();

            if (ItemHelper.areStacksEqual(machineRecipe.getInput().getInputs(), recipe.getInput().getInputs())) {
                if (ItemHelper.areStacksEqual(machineRecipe.getOutput(), recipe.getOutputs())) {
                    iterator.remove();
                    removed = true;
                }
            }
        }
        if (removed) {
            JEIHelper.removeRecipe(wrapper, category);
        }
    }

    public static class Add extends BaseListAddition<IC2Recipe> {

        private HashMap<IC2Recipe, IORecipeWrapper> recipeJEI = new HashMap<>();
        private String category;
        private IBasicMachineRecipeManager manager;

        public Add(List<IC2Recipe> list, IBasicMachineRecipeManager manager, String category) {
            this(list, manager, category, IRecipeWrapperGenerator.basicMachine);
        }

        public Add(List<IC2Recipe> list, IBasicMachineRecipeManager manager, String category, IRecipeWrapperGenerator<IBasicMachineRecipeManager> generator) {
            super(category, list);
            this.manager = manager;
            this.category = category;

            for (IC2Recipe recipe : list) {
                add(recipe, recipeJEI, manager, generator, category);
            }
        }

        @Override
        protected String getRecipeInfo(IC2Recipe recipe) {
            return recipe.getOutputs().toString();
        }

        @Override
        public String getJEICategory(IC2Recipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (IC2Recipe recipe : recipeJEI.keySet()) {
                remove(recipe, recipeJEI.get(recipe), manager, category);
            }
        }
    }

    public static class Remove extends BaseListRemoval<IC2Recipe> {

        private HashMap<IC2Recipe, IORecipeWrapper> recipeJEI = new HashMap<>();
        private String category;
        private IBasicMachineRecipeManager manager;
        private IRecipeWrapperGenerator<IBasicMachineRecipeManager> generator;

        public Remove(List<IC2Recipe> list, IBasicMachineRecipeManager manager, String category) {
            this(list, manager, category, IRecipeWrapperGenerator.basicMachine);
        }

        public Remove(List<IC2Recipe> list, IBasicMachineRecipeManager manager, String category, IRecipeWrapperGenerator<IBasicMachineRecipeManager> generator) {
            super(category, list);
            this.category = category;
            this.manager = manager;
            this.generator = generator;

            for (IC2Recipe recipe : list) {
                for (IORecipeWrapper recipeJEI : JEIHelper.<IORecipeWrapper>getRecipeWrappers(category)) {

                    boolean removed = false;
                    if (ItemHelper.areStacksEqual(recipeJEI.getOutputs(), recipe.getOutputs())) {
                        for (List<ItemStack> inputItems : recipeJEI.getInputs()) {
                            if (ItemHelper.areStacksEqual(inputItems, recipe.getInput().getInputs())) {
                                this.recipeJEI.put(recipe, recipeJEI);
                                remove(recipe, this.recipeJEI.get(recipe), manager, category);
                                removed = true;
                                break;
                            }
                        }
                    }
                    if (removed) break;
                }
            }
        }

        @Override
        protected String getRecipeInfo(IC2Recipe recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(IC2Recipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (IC2Recipe recipe : recipeJEI.keySet()) {
                add(recipe, recipeJEI, manager, generator, category);
            }
        }
    }
}
