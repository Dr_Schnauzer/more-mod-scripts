package de.dr_schnauzer.project_unstable.mms.bloodmagic.classes;

import WayofTime.bloodmagic.api.recipe.AlchemyTableRecipe;
import WayofTime.bloodmagic.api.registry.AlchemyTableRecipeRegistry;
import WayofTime.bloodmagic.compat.jei.alchemyTable.AlchemyTableRecipeJEI;
import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.helpers.StackHelper;
import com.blamejared.mtlib.utils.BaseListAddition;
import com.blamejared.mtlib.utils.BaseListRemoval;
import com.google.common.collect.Lists;
import de.dr_schnauzer.project_unstable.mms.util.JEIHelper;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

@ZenClass("mods.bloodmagic.AlchemyTable")
public class AlchemyTable {

    private static final String category = "bloodmagic:salchemyTable";

    @ZenMethod
    public static void add(IItemStack output, int lpDrained, int ticksRequired, int tierRequired, IIngredient[] input) {
        AlchemyTableRecipe recipe = new AlchemyTableRecipe(InputHelper.toStack(output), lpDrained, ticksRequired, tierRequired, InputHelper.toObjects(input));
        MineTweakerAPI.apply(new Add(Collections.singletonList(recipe)));
    }

    @ZenMethod
    public static void remove(IItemStack output) {
        AlchemyTableRecipe recipeToRemove = null;
        for (AlchemyTableRecipe recipe : AlchemyTableRecipeRegistry.getRecipeList()) {
            if (StackHelper.areEqual(recipe.getRecipeOutput(Lists.newArrayList()), InputHelper.toStack(output))) {
                recipeToRemove = recipe;
                break;
            }
        }
        if (recipeToRemove != null) {
            MineTweakerAPI.apply(new Remove(Collections.singletonList(recipeToRemove)));
        }
    }

    @ZenMethod
    public static void remove(IItemStack output, IIngredient[] input) {
        List<ItemStack> inputList = new ArrayList<>();
        Collections.addAll(inputList, InputHelper.toStacks(InputHelper.toStacks(input)));

        AlchemyTableRecipe recipeToRemove = null;
        for (AlchemyTableRecipe recipe : AlchemyTableRecipeRegistry.getRecipeList()) {
            if (StackHelper.areEqual(recipe.getRecipeOutput(inputList), InputHelper.toStack(output))) {
                recipeToRemove = recipe;
                break;
            }
        }
        if (recipeToRemove != null) {
            MineTweakerAPI.apply(new Remove(Collections.singletonList(recipeToRemove)));
        }
    }

    private static class Add extends BaseListAddition<AlchemyTableRecipe> {

        private HashMap<AlchemyTableRecipe, AlchemyTableRecipeJEI> recipeJEI = new HashMap<>();

        protected Add(List<AlchemyTableRecipe> list) {
            super("Alchemy Table", list);
            for (AlchemyTableRecipe recipe : list) {
                recipeJEI.put(recipe, new AlchemyTableRecipeJEI(recipe));
                AlchemyTableRecipeRegistry.registerRecipe(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }

        @Override
        protected String getRecipeInfo(AlchemyTableRecipe recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(AlchemyTableRecipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (AlchemyTableRecipe recipe : recipeJEI.keySet()) {
                AlchemyTableRecipeRegistry.removeRecipe(recipe);
                JEIHelper.removeRecipe(recipeJEI.get(recipe), category);
            }
        }
    }

    private static class Remove extends BaseListRemoval<AlchemyTableRecipe> {

        private HashMap<AlchemyTableRecipe, AlchemyTableRecipeJEI> recipeJEI = new HashMap<>();

        protected Remove(List<AlchemyTableRecipe> list) {
            super("Alchemy Table", list);
            for (AlchemyTableRecipe recipe : list) {

                for (AlchemyTableRecipeJEI recipeJEI : JEIHelper.<AlchemyTableRecipeJEI>getRecipeWrappers(category)) {
                    if (recipeJEI.getRecipe().equals(recipe)) {
                        this.recipeJEI.put(recipe, recipeJEI);
                        AlchemyTableRecipeRegistry.removeRecipe(recipe);
                        JEIHelper.removeRecipe(recipeJEI, category);
                    }
                }
            }
        }

        @Override
        protected String getRecipeInfo(AlchemyTableRecipe recipe) {
            return recipe.toString();
        }

        @Override
        public String getJEICategory(AlchemyTableRecipe recipe) {
            return category;
        }

        @Override
        public boolean canUndo() {
            return true;
        }

        @Override
        public void undo() {
            super.undo();
            for (AlchemyTableRecipe recipe : recipeJEI.keySet()) {
                AlchemyTableRecipeRegistry.registerRecipe(recipe);
                JEIHelper.addRecipe(recipeJEI.get(recipe), category);
            }
        }
    }
}
