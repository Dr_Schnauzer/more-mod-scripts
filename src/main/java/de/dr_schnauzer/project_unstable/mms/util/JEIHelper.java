package de.dr_schnauzer.project_unstable.mms.util;

import mezz.jei.Internal;
import mezz.jei.api.recipe.IRecipeWrapper;

import java.util.Collections;
import java.util.List;

public class JEIHelper {

    public static <T extends IRecipeWrapper> void addRecipe(T recipeWrapper, String category) {
        if (Internal.getRuntime() != null && Internal.getRuntime().getRecipeRegistry() != null) {
            Internal.getRuntime().getRecipeRegistry().addRecipe(recipeWrapper, category);
        }
    }

    public static <T extends IRecipeWrapper> void removeRecipe(T recipeWrapper, String category) {
        if (Internal.getRuntime() != null && Internal.getRuntime().getRecipeRegistry() != null) {
            Internal.getRuntime().getRecipeRegistry().removeRecipe(recipeWrapper, category);
        }
    }

    @SuppressWarnings("unchecked")
    public static <T extends IRecipeWrapper> List<T> getRecipeWrappers(String category) {
        if (Internal.getRuntime() != null && Internal.getRuntime().getRecipeRegistry() != null) {
            return Internal.getRuntime().getRecipeRegistry().getRecipeWrappers(Internal.getRuntime().getRecipeRegistry().getRecipeCategories(Collections.singletonList(category)).get(0));
        }
        return Collections.EMPTY_LIST;
    }
}
