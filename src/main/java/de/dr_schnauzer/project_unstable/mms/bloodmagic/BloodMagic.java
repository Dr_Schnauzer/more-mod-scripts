package de.dr_schnauzer.project_unstable.mms.bloodmagic;

import de.dr_schnauzer.project_unstable.mms.bloodmagic.classes.*;
import minetweaker.MineTweakerAPI;

public class BloodMagic {

    public static void register() {

        MineTweakerAPI.registerClass(AlchemyTable.class);
        MineTweakerAPI.registerClass(Altar.class);
        MineTweakerAPI.registerClass(HellfireForge.class);
        //TODO MineTweakerAPI.registerClass(AlchemyArray.class);
    }
}
