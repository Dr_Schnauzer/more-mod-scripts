package de.dr_schnauzer.project_unstable.mms.ic2.classes;

import com.blamejared.mtlib.helpers.InputHelper;
import com.blamejared.mtlib.helpers.LogHelper;
import com.blamejared.mtlib.helpers.StackHelper;
import de.dr_schnauzer.project_unstable.mms.ic2.BaseMachineRecipe;
import de.dr_schnauzer.project_unstable.mms.ic2.IC2Recipe;
import ic2.api.recipe.IRecipeInput;
import ic2.api.recipe.MachineRecipe;
import ic2.api.recipe.Recipes;
import minetweaker.MineTweakerAPI;
import minetweaker.api.item.IIngredient;
import minetweaker.api.item.IItemStack;
import net.minecraft.item.ItemStack;
import stanhebben.zenscript.annotations.ZenClass;
import stanhebben.zenscript.annotations.ZenMethod;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

@ZenClass("mods.ic2.MetalFormer")
public class MetalFormer extends BaseMachineRecipe {

    private static final String categoryExtruding = "metal_former0";
    private static final String categoryRolling = "metal_former1";
    private static final String categoryCutting = "metal_former2";

    @ZenMethod
    public static void addExtruding(IItemStack output, IIngredient input) {
        IC2Recipe ic2Recipe = new IC2Recipe(new ItemStack[]{InputHelper.toStack(output)}, input, false);
        if (ic2Recipe.isValid()) {
            MineTweakerAPI.apply(new Add(Collections.singletonList(ic2Recipe), Recipes.metalformerExtruding, categoryExtruding));
        }
    }

    @ZenMethod
    public static void removeExtrudingByOutput(IIngredient output) {
        List<IC2Recipe> recipesToRemove = new ArrayList<>();
        for (MachineRecipe<IRecipeInput, Collection<ItemStack>> recipe : Recipes.metalformerExtruding.getRecipes()) {
            boolean match = false;
            for (IItemStack outputStack : output.getItems()) {
                for (ItemStack stack : recipe.getOutput()) {
                    if (StackHelper.areEqual(InputHelper.toStack(outputStack), stack)) {
                        recipesToRemove.add(new IC2Recipe(recipe));
                        match = true;
                        break;
                    }
                }
                if (match) break;
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove, Recipes.metalformerExtruding, categoryExtruding));
        } else {
            LogHelper.logError("no matching Metal Former Extruding recipe found for output " + output);
        }
    }

    @ZenMethod
    public static void removeExtrudingByInput(IIngredient input) {
        List<IC2Recipe> recipesToRemove = new ArrayList<>();
        for (MachineRecipe<IRecipeInput, Collection<ItemStack>> recipe : Recipes.metalformerExtruding.getRecipes()) {
            for (IItemStack inputStack : input.getItems()) {
                for (ItemStack stack : recipe.getInput().getInputs()) {
                    if (StackHelper.areEqual(stack, InputHelper.toStack(inputStack))) {
                        recipesToRemove.add(new IC2Recipe(recipe));
                    }
                }
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove, Recipes.metalformerExtruding, categoryExtruding));
        } else {
            LogHelper.logError("no matching Metal Former Extruding recipe found for input " + input);
        }
    }

    @ZenMethod
    public static void addRolling(IItemStack output, IIngredient input) {
        IC2Recipe ic2Recipe = new IC2Recipe(new ItemStack[]{InputHelper.toStack(output)}, input, false);
        if (ic2Recipe.isValid()) {
            MineTweakerAPI.apply(new Add(Collections.singletonList(ic2Recipe), Recipes.metalformerRolling, categoryRolling));
        }
    }

    @ZenMethod
    public static void removeRollingByOutput(IIngredient output) {
        List<IC2Recipe> recipesToRemove = new ArrayList<>();
        for (MachineRecipe<IRecipeInput, Collection<ItemStack>> recipe : Recipes.metalformerRolling.getRecipes()) {
            boolean match = false;
            for (IItemStack outputStack : output.getItems()) {
                for (ItemStack stack : recipe.getOutput()) {
                    if (StackHelper.areEqual(InputHelper.toStack(outputStack), stack)) {
                        recipesToRemove.add(new IC2Recipe(recipe));
                        match = true;
                        break;
                    }
                }
                if (match) break;
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove, Recipes.metalformerRolling, categoryRolling));
        } else {
            LogHelper.logError("no matching Metal Former Rolling recipe found for output " + output);
        }
    }

    @ZenMethod
    public static void removeRollingByInput(IIngredient input) {
        List<IC2Recipe> recipesToRemove = new ArrayList<>();
        for (MachineRecipe<IRecipeInput, Collection<ItemStack>> recipe : Recipes.metalformerRolling.getRecipes()) {
            for (IItemStack inputStack : input.getItems()) {
                for (ItemStack stack : recipe.getInput().getInputs()) {
                    if (StackHelper.areEqual(stack, InputHelper.toStack(inputStack))) {
                        recipesToRemove.add(new IC2Recipe(recipe));
                    }
                }
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove, Recipes.metalformerRolling, categoryRolling));
        } else {
            LogHelper.logError("no matching Metal Former Rolling recipe found for input " + input);
        }
    }

    @ZenMethod
    public static void addCutting(IItemStack output, IIngredient input) {
        IC2Recipe ic2Recipe = new IC2Recipe(new ItemStack[]{InputHelper.toStack(output)}, input, false);
        if (ic2Recipe.isValid()) {
            MineTweakerAPI.apply(new Add(Collections.singletonList(ic2Recipe), Recipes.metalformerCutting, categoryCutting));
        }
    }

    @ZenMethod
    public static void removeCuttingByOutput(IIngredient output) {
        List<IC2Recipe> recipesToRemove = new ArrayList<>();
        for (MachineRecipe<IRecipeInput, Collection<ItemStack>> recipe : Recipes.metalformerCutting.getRecipes()) {
            boolean match = false;
            for (IItemStack outputStack : output.getItems()) {
                for (ItemStack stack : recipe.getOutput()) {
                    if (StackHelper.areEqual(InputHelper.toStack(outputStack), stack)) {
                        recipesToRemove.add(new IC2Recipe(recipe));
                        match = true;
                        break;
                    }
                }
                if (match) break;
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove, Recipes.metalformerCutting, categoryCutting));
        } else {
            LogHelper.logError("no matching Metal Former Cutting recipe found for output " + output);
        }
    }

    @ZenMethod
    public static void removeCuttingByInput(IIngredient input) {
        List<IC2Recipe> recipesToRemove = new ArrayList<>();
        for (MachineRecipe<IRecipeInput, Collection<ItemStack>> recipe : Recipes.metalformerCutting.getRecipes()) {
            for (IItemStack inputStack : input.getItems()) {
                for (ItemStack stack : recipe.getInput().getInputs()) {
                    if (StackHelper.areEqual(stack, InputHelper.toStack(inputStack))) {
                        recipesToRemove.add(new IC2Recipe(recipe));
                    }
                }
            }
        }
        if (!recipesToRemove.isEmpty()) {
            MineTweakerAPI.apply(new Remove(recipesToRemove, Recipes.metalformerCutting, categoryCutting));
        } else {
            LogHelper.logError("no matching Metal Former Cutting recipe found for input " + input);
        }
    }
}
